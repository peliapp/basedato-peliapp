-- --------------------------------------------------------
-- Host:                         db-des-pcyll.ciggpldtewva.us-east-1.rds.amazonaws.com
-- Versión del servidor:         5.7.22-log - Source distribution
-- SO del servidor:              Linux
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para peliapp
DROP DATABASE IF EXISTS `peliapp`;
CREATE DATABASE IF NOT EXISTS `peliapp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `peliapp`;

-- Volcando estructura para tabla peliapp.detalle_pelicula
DROP TABLE IF EXISTS `detalle_pelicula`;
CREATE TABLE IF NOT EXISTS `detalle_pelicula` (
  `idturno` int(11) NOT NULL,
  `idPelicula` int(11) NOT NULL,
  `valor` tinyint(1) NOT NULL,
  KEY `idturno` (`idturno`),
  KEY `idPelicula` (`idPelicula`),
  CONSTRAINT `detalle_pelicula_ibfk_1` FOREIGN KEY (`idturno`) REFERENCES `turno` (`idturno`),
  CONSTRAINT `detalle_pelicula_ibfk_2` FOREIGN KEY (`idPelicula`) REFERENCES `pelicula` (`idPelicula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla peliapp.estado
DROP TABLE IF EXISTS `estado`;
CREATE TABLE IF NOT EXISTS `estado` (
  `idestado` int(11) NOT NULL,
  `varNombrEstado` varchar(20) NOT NULL,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla peliapp.pelicula
DROP TABLE IF EXISTS `pelicula`;
CREATE TABLE IF NOT EXISTS `pelicula` (
  `idPelicula` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `publicacion` varchar(50) DEFAULT NULL,
  `urlImagen` varchar(500) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `update_at` varchar(45) DEFAULT NULL,
  `idestado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPelicula`),
  KEY `idestado_idx` (`idestado`),
  CONSTRAINT `pelicula_ibfk_1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla peliapp.turno
DROP TABLE IF EXISTS `turno`;
CREATE TABLE IF NOT EXISTS `turno` (
  `idturno` int(11) NOT NULL AUTO_INCREMENT,
  `varfechturno` varchar(20) NOT NULL,
  `idestado` int(11) NOT NULL,
  PRIMARY KEY (`idturno`),
  KEY `idestado` (`idestado`),
  CONSTRAINT `turno_ibfk_1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
